function fem=genmesh(fem,level)

%This file performs the following tasks:
%::generates the geometry of a square domain with a single circular hole
%::refines the domain and generates the connectivity arrays for a
%mesh of conforming linear triangles
%::provides the index sets for periodic and Neumann boundaries

global holes

%GENERATE MESH
ax=-pi;bx=pi;cy=-pi;dy=pi;
if fem.problem==1%single centered circular hole
    R1=[3 4 ax bx bx ax cy cy dy dy];gd=R1(:);
    C1=[1 holes.cx holes.cy holes.r 0 0 0 0 0 0];gd=[gd C1(:)];
    ns=char('R1','C1');
    sf='R1-C1';
    Hmax=[];
end
geom=decsg(gd,sf,ns');

[fem.mesh,fem.bdy]=P1gen(geom,level,Hmax);

N=max(max(fem.mesh.t));

%setup periodic boundaries: bdy_west, bdy_east, bdy_north, bdy_south:
%%Changed comment to describe all boundary conditions 29.10.2018
i1=unique(find(fem.mesh.p(1,:)==ax & fem.mesh.p(2,:)>cy & fem.mesh.p(2,:)<dy));
[dummy,j1]=sort(fem.mesh.p(2,i1));i1=i1(j1);
i2=unique(find(fem.mesh.p(1,:)==bx & fem.mesh.p(2,:)>cy & fem.mesh.p(2,:)<dy));
[dummy,j2]=sort(fem.mesh.p(2,i2));i2=i2(j2);
i3=unique(find(fem.mesh.p(2,:)==cy & fem.mesh.p(1,:)>ax & fem.mesh.p(1,:)<bx));
[dummy,j3]=sort(fem.mesh.p(1,i3));i3=i3(j3);
i4=unique(find(fem.mesh.p(2,:)==dy & fem.mesh.p(1,:)>ax & fem.mesh.p(1,:)<bx));
[dummy,j4]=sort(fem.mesh.p(1,i4));i4=i4(j4);
%corners
i5=unique(find(fem.mesh.p(1,:)==ax & fem.mesh.p(2,:)==cy));
[dummy,j5]=sort(fem.mesh.p(1,i5));i5=i5(j5);
i6=unique(find(fem.mesh.p(1,:)==ax & fem.mesh.p(2,:)==dy));
[dummy,j6]=sort(fem.mesh.p(1,i6));i6=i6(j6);
i7=unique(find(fem.mesh.p(1,:)==bx & fem.mesh.p(2,:)==cy));
[dummy,j7]=sort(fem.mesh.p(1,i7));i7=i7(j7);
i8=unique(find(fem.mesh.p(1,:)==bx & fem.mesh.p(2,:)==dy));
[dummy,j8]=sort(fem.mesh.p(1,i8));i8=i8(j8);

fem.bdyw=i1;fem.bdye=i2;fem.bdys=i3;fem.bdyn=i4;
fem.bdyc1=i5;fem.bdyc2=i6;fem.bdyc3=i7;fem.bdyc4=i8;

fem.nbw=length(fem.bdyw);
fem.nbs=length(fem.bdys);
fem.nbc1=length(fem.bdyc1);

fem.bdy=[fem.bdye fem.bdyw fem.bdys fem.bdyn fem.bdyc1 fem.bdyc2 fem.bdyc3 fem.bdyc4];
fem.int=setdiff(1:N,fem.bdy);

%%------------------------------------------------------------------
function [mesh,bdy]=P1gen(domain,level,Hmax)
%This routine generates a mesh of linear triangles 
%for the domain defined in 'domain'

if ~exist('level'), level=1;end;
if ~exist('Hmax') | isempty(Hmax)
  [mesh.p,mesh.e,mesh.t]=initmesh(domain,'JiggleIter',3);
else
  [mesh.p,mesh.e,mesh.t]=initmesh(domain,'Hmax',Hmax,'JiggleIter',3);
end
for i=1:level
  [mesh.p,mesh.e,mesh.t]=refinemesh(domain,mesh.p,mesh.e,mesh.t);
end;
mesh.p=jigglemesh(mesh.p,mesh.e,mesh.t);
bdy=unique(mesh.e(1:2,find(mesh.e(7,:)==0 | mesh.e(6,:)==0)));

