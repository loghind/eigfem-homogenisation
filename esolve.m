v1=ones(length(K.M),1);
if echoice==0%use eigs
    opts.v0=v1;opts.tol=etol;
    [v1,lam,flag]=eigs(K.A,K.M,1,lq,opts);   
elseif echoice==1%use shiftinvert
    [v1,lam,err,its,flag]=shiftinvert(K.A,K.M,100,v1,eguess,etol);    
end
fprintf('%4.2f\t (%4.2f, %4.2f)\t %4.6f \n', holes.r, q(1), q(2), real(lam))

if lam+norm(q)^2<0,flag=1;end;

function [v,lam,err,its,flag]=shiftinvert(A,M,kmax,v,s,tol)

%
%This function computes an approximation to the eigenvector 
%corresponding to the dominant eigenvalue of A using a shift s,
%v as starting vector and kmax iterations.

n=length(A);its=0;err=1;flag=0;
if ~exist('v') | isempty(v),v=ones(n,1);end
if ~exist('kmax'),kmax=10;end
if ~exist('s'),s=0;end
if ~exist('tol'),tol=1e-3;end

v=v/norm(v);Mv=M*v;
while err>tol & its<kmax
    its=its+1;
    v=(A-s*M)\Mv;
    v=v/norm(v);
    Mv=M*v;
    lam=v'*A*v/(v'*Mv);s=lam;
    err=norm(A*v-lam*Mv);
end
if err>tol, flag=1;end    
end
    