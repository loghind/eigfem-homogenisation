N=max(max(fem.mesh.t));
%GAUSS POINTS
ng=4;
[psi,eta,w]=gausspoints(ng);

%SETUP SHAPE FUNCTIONS AND DERIVATIVES
[phi,dphidx,dphidy] = shapes(psi,eta,1);
p=size(phi,2);
%PREPARE FOR ASSEMBLY
%elemental arrays for p derivatives of order 0,1,2
me=kron(ones(p,1),phi).*kron(ones(1,p),phi(:));me=reshape(me,ng,p^2);
uexx=kron(ones(p,1),dphidx).*kron(ones(1,p),dphidx(:));uexx=reshape(uexx,ng,p^2);
ueyy=kron(ones(p,1),dphidy).*kron(ones(1,p),dphidy(:));ueyy=reshape(ueyy,ng,p^2);
uexy=kron(ones(p,1),dphidx).*kron(ones(1,p),dphidy(:));uexy=reshape(uexy,ng,p^2);
ueyx=kron(ones(p,1),dphidy).*kron(ones(1,p),dphidx(:));ueyx=reshape(ueyx,ng,p^2);
uexyx=uexy+ueyx;
uexx=reshape(w*uexx,p,p);
uexy=reshape(w*uexy,p,p);
ueyy=reshape(w*ueyy,p,p);
uexyx=reshape(w*uexyx,p,p);
me=reshape(w*me,p,p);

%Corner point indices
for i=1:p
  eval(['it',num2str(i),'=fem.mesh.t(',num2str(i),',:);'])
end

detadx =fem.mesh.p(2,it1)-fem.mesh.p(2,it2); 
detady =fem.mesh.p(1,it2)-fem.mesh.p(1,it1); 
dpsidx =fem.mesh.p(2,it3)-fem.mesh.p(2,it1); 
dpsidy =fem.mesh.p(1,it1)-fem.mesh.p(1,it3); 
jac=dpsidx.*detady-dpsidy.*detadx; 

d1=(dpsidx.^2+dpsidy.^2)./jac; 
d12=(dpsidy.*detady+dpsidx.*detadx)./jac;
d2=(detadx.^2+detady.^2)./jac; 

fprintf('Assembling matrices...');
%--------------------assemble laplacian matrix------------------------
K.A0=sparse(N,N);K.M0=sparse(N,N);
for i=1:p
  for j=1:p
    ae=uexx(i,j)*d1+uexyx(i,j)*d12+ueyy(i,j)*d2;
    K.A0=K.A0-sparse(eval(['it',num2str(i)]),eval(['it',num2str(j)]),ae,N,N);
    K.M0=K.M0+sparse(eval(['it',num2str(i)]),eval(['it',num2str(j)]),me(i,j)*jac,N,N);
  end
end
%---------------------assemble convective matrices-----------------------
K.S1=sparse(N,N);K.S2=sparse(N,N);
sex=kron(ones(p,1),dphidx).*kron(ones(1,p),phi(:));sex=reshape(sex,ng,p^2); %=grad phi*psi;
sey=kron(ones(p,1),dphidy).*kron(ones(1,p),phi(:));sey=reshape(sey,ng,p^2);  
sex=reshape(w*sex,p,p);sey=reshape(w*sey,p,p);
for i=1:p
    for j=1:p
        se=sex(i,j)*dpsidx+sey(i,j)*detadx;
        K.S1=K.S1-sparse(eval(['it',num2str(j)]),eval(['it',num2str(i)]),se,N,N);
        se=sex(i,j)*dpsidy+sey(i,j)*detady;
        K.S2=K.S2-sparse(eval(['it',num2str(j)]),eval(['it',num2str(i)]),se,N,N);
    end
end
sex=kron(ones(p,1),phi).*kron(ones(1,p),dphidx(:));sex=reshape(sex,ng,p^2); %=grad psi*phi;
sey=kron(ones(p,1),phi).*kron(ones(1,p),dphidy(:));sey=reshape(sey,ng,p^2);
sex=reshape(w*sex,p,p);sey=reshape(w*sey,p,p);
for i=1:p
    for j=1:p
        se=sex(i,j)*dpsidx+sey(i,j)*detadx;
        K.S1=K.S1+sparse(eval(['it',num2str(j)]),eval(['it',num2str(i)]),se,N,N);
        se=sex(i,j)*dpsidy+sey(i,j)*detady;
        K.S2=K.S2+sparse(eval(['it',num2str(j)]),eval(['it',num2str(i)]),se,N,N);
    end
end

%prepare matrices for bc-setup
Ew=speye(fem.nbw,fem.nbw);
Es=speye(fem.nbs,fem.nbs);
Ei=speye(N-2*fem.nbw-2*fem.nbs-4);
Ec=speye(fem.nbc1,fem.nbc1);

K.P=sparse(N,N-fem.nbw-fem.nbs-fem.bdyc1);
K.P(fem.int,fem.int)=Ei;
K.P(fem.bdyw,fem.bdyw)=Ew;K.P(fem.bdys,fem.bdys)=Es;
K.P(fem.bdye,fem.bdyw)=Ew;K.P(fem.bdyn,fem.bdys)=Es;
K.P(fem.bdyn,fem.bdys)=Es;
K.P(fem.bdyc1,fem.bdyc1)=Ec;
K.P(fem.bdyc2,fem.bdyc1)=Ec;
K.P(fem.bdyc3,fem.bdyc1)=Ec;
K.P(fem.bdyc4,fem.bdyc1)=Ec;

K.P=K.P(:,[fem.int fem.bdyw fem.bdys fem.bdyc1]);

K.M=K.P'*K.M0*K.P;
K.A0=K.P'*K.A0*K.P;
K.S1=K.P'*K.S1*K.P;
K.S2=K.P'*K.S2*K.P;

fprintf('\b.done!\n')
%-------------------------------------------
function [phi,dphidx,dphidy] = shapes(x,y,p);

L1=1-x-y;dxL1=-1+0*x;dyL1=-1+0*x;
L2=x;dxL2=1+0*x;dyL2=0+0*x;
L3=y;dxL3=0+0*x;dyL3=1+0*x;

phi1=[L1 L2 L3];

if p==1%linear triangle
  phi=phi1;
  dphidx=[dxL1 dxL2 dxL3];
  dphidy=[dyL1 dyL2 dyL3];
end
end
%----------------------------------------------
function [psi,eta,w]=gausspoints(n)

%function gausspoints(n)
%This file contains the Gauss points as found in Strang+Fix, pp184
%n=3   -->degree of precision 2
%n=4   -->degree of precision 3


if n==3 %degree of precision 2
	psi=[2/3 1/6 1/6];
	eta=[1/6 1/6 2/3];
	w=[1/3 1/3 1/3];
elseif n==4 %degree of precision 3
	psi=[1/3 1/5 3/5 1/5];
	eta=[1/3 1/5 1/5 3/5];
	w=[-27/48 25/48 25/48 25/48];
else
	error('This degree of precision is not available')
end;
w=w/2;
psi=psi';
eta=eta';
end
%-----------------------------------------------








