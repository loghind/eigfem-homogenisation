%Run this script to compute the eigenvalues f(q) described in
%eqn. (9) in the manuscript 
%Diffusion in arrays of obstacles: beyond homogenisation 
%by Yahya Farah, Daniel Loghin, Alexandra Tzella and Jacques Vanneste.
%---------------------------------------------------------------------
clear all
global holes
%%---------------GEOM setup---------------------
fem.problem=1;%used in genmesh.m
holes.cx=0;holes.cy=0;%center coords
holes.r=0.1; %radius; max value: 0.5 DILUTE
%%---------------FEM setup---------------------
level=3;%refinement level
fem=genmesh(fem,level);%mesh generation
assemb_elliptic%FEM assembly
disp(['Problem size = ',num2str(N),'.'])
%%----------------EIG loop setup------------------------------------
qvec=[0:0.1:1 1.1:0.1:10];%define range of q
[q1,q2]=meshgrid(qvec);

%reorder the indices to provide better initial guesses for eigensolver
[ix,iy]=meshgrid(1:length(qvec));
ind=rot90(ix+sqrt(-1)*iy);
IND=[];flip=-1;
for k=-length(qvec)+1:length(qvec)-1
    dk=diag(ind,k);
    if flip==-1
        IND=[IND;flipud(dk)];
    else
        IND=[IND;dk];
    end
    flip=-flip;
end 
LQ=zeros(size(q1)); 
lq=0;
%choice of eigensolver
echoice=1;etol=1e-12;%shift-and-invert
LQ(1,1)=0;V(1,1,:)=ones(1,1,length(K.P*K.M));
%%-----------------------EIG iteration-------------------------------------
fprintf('-----------------------------------------------------\n');
fprintf('r\t    (q1, q2)\t       f(qi) \n');
fprintf('-----------------------------------------------------\n');
tic
for k=2:length(IND)
    
    i=real(IND(k));j=imag(IND(k));
    q=[q1(i,j); q2(i,j)];
    
    K.A=K.A0+q(1)*K.S1+q(2)*K.S2;
    
    eguess=lq;%initial guess for eigensolver
    
    esolve
    
    if flag,disp('Negative eigenvalue!');break; end
    
    lq=lam;%scaled eigenvalue
    
    v1=K.P*v1;%full eigenvector
    
    flagv=0;value=v1(1);
    
    if value<0
        index = find(v1 > 0, 1, 'first');
    else 
        index = find(v1 < 0, 1, 'first');
    end    
        
    if length(index)~=0, disp('Sign change of eigenvector!');flagv=1;break;end
    
    LQ(i,j)=lq+norm(q)^2;%store eigenvalue 
    V(i,j,:)=v1;%store eigenvector
    
end
fprintf('-----------------------------------------------------\n');
toc

%%-------------------PLOT f(q)-----------------------------
if flag==0 %if no issues, plot!
    figure(77),surf(q1,q2,LQ);rotate3d on;
    box on;grid on;xlabel('q1');ylabel('q2');zlabel('f(q)');   
end
